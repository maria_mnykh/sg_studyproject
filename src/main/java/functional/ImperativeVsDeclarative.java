package src.main.java.functional;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ImperativeVsDeclarative {
    public static void main(String[] args) {

        //Imperative

        List<Integer> integerList = Arrays.asList(2, 3, 45, 6, 7, 8);
        Integer count1 = doSomething(integerList);
        System.out.println(count1);

        //Declarative

        Integer count = (int) integerList.stream()
                .map(i -> i * 1)
                .filter(i -> (i % 3) == 0)
                .count();
        System.out.println(count);
    }

    private static int doSomething(List<Integer> list) {
        Integer count = 0;
        List<Integer> filteredList = new ArrayList<>();
        for (Integer integer : list) {
            filteredList.add(integer * 2);
        }
        for (int i = 0; i < filteredList.size(); i++) {
            if ((filteredList.get(i) % 3) == 0) {
                count += 1;
            }
        }
        return count;
    }

}