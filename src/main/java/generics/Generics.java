package src.main.java.generics;

public class Generics<T> implements TestInterface {
    private T t;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public static void main(String[] args) {

        Generics<Integer> integerGenerics = new Generics<Integer>();
//        integerGenerics.setT(new Integer(90));
//        System.out.println(integerGenerics.getT());
//
//        Generics<String> stringGenerics = new Generics<String>();
//        stringGenerics.setT("Test");
//        System.out.println(stringGenerics.getT());
//
//        System.out.println(stringGenerics.compareTo(integerGenerics));


        //List<Integer> list = new ArrayList<Integer>();

        Integer[] intArray = {1, 2, 3, 4, 5};
        Double[] doubleArray = {1.1, 2.2, 3.3, 4.4};
        Character[] charArray = {'H', 'E', 'L', 'L', 'O'};
        printArray(intArray);
        printArray(doubleArray);
        printArray(charArray);

    }

    public static <E> void printArray(E[] inputArray) {
        for (E element : inputArray) {
            System.out.printf("%s ", element);
        }
        System.out.println();
    }

    public boolean compareTo(Object o) {
        if (o.equals(t)) {
            return true;
        } else {
            return false;
        }
    }
}

interface TestInterface<T> {
    boolean compareTo(T o);
}