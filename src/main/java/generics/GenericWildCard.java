package src.main.java.generics;


import src.main.java.generics.cars.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GenericWildCard {
    public static void main(String[] args) {

        List<Vehicle> vehicles = Arrays.asList(
                new Vehicle("Tractor", "Model K", 2019, 15000),
                new Vehicle("Сombine", "Model K10", 2009, 18000)
        );
        List<Car> cars = Arrays.asList(
                new Car("Tesla", "Model X", 2018, 40000, "200 km/h"),
                new Car("Tesla", "Model S", 2017, 30000, "190 km/h")
        );

        List<Lamborghini> lamborghinis = Arrays.asList(
                new Lamborghini("Lamborghini", "Urus", 2021, 80000, "220 km/h", "red"),
                new Lamborghini("Lamborghini", "Huracan", 2019, 90000, "240 km/h", "yellow"),
                new Lamborghini("Lamborghini", "Aventador", 2021, 90000, "230 km/h", "blu")
        );

        List<Object> objects = Arrays.asList(
                new Object(),
                new Object()
        );

//        printObjects(vehicles);
//        printObjects(cars);
//        printObjects(lamborghinis);


//        System.out.println(filterByName(cars, "Tesla"));
//        System.out.println(filterByName(lamborghinis, "Lamborghini"));
//        System.out.println(filterByName(vehicles, "Tractor"));


        //System.out.println(filterByColor(lamborghinis, "red"));
        //System.out.println(filterByColor(cars, "red"));

//        System.out.println(filterBySpeed(cars, "200 km/h"));
//        System.out.println(filterBySpeed(lamborghinis, "220 km/h"));
//        System.out.println(filterBySpeed(vehicles, "220 km/h"));


//        getClassName(objects);
//        getClassName(vehicles);
//        getClassName(cars);

        System.out.println(getCount(vehicles));
        System.out.println(getCount(cars));
        System.out.println(getCount(lamborghinis));
        System.out.println(getCount(objects));

    }

    //will work for all superclasses of Lamborghini
    public static void printObjects(List<? super Lamborghini> list) { // Object, Vehicle, Car, Lamb
        for (Object object : list) {
            System.out.println(object.toString());
        }
    }

    //here will work for all subclasses of Vehicle
    private static List<?> filterByName(List<? extends Vehicle> list, String name) {
        List<Object> filteredList = new ArrayList<Object>();
        for (Vehicle vehicle : list) {
            if (vehicle.getName().equals(name)) {
                filteredList.add(vehicle);
            }
        }
        return filteredList;
    }

    //here will work only for Lamborghini as it does not have any child classes
    private static List<?> filterByColor(List<? extends Lamborghini> list, String color) {
        List<Object> filteredList = new ArrayList<Object>();
        for (Lamborghini lamborghini : list) {
            if (lamborghini.getColor().equals(color)) {
                filteredList.add(lamborghini);
            }
        }
        return filteredList;
    }

    //here will work for Car and Lamborghini classes
    private static List<?> filterBySpeed(List<? extends Car> list, String speed) {
        List<Object> filteredList = new ArrayList<Object>();
        for (Car car : list) {
            if (car.getSpeed().equals(speed)) {
                filteredList.add(car);
            }
        }
        return filteredList;
    }

    //here will work for Vehicle and Object
    private static void getClassName(List<? super Vehicle> list) {
        for (Object object : list) {
            System.out.println(object.getClass().getName());
        }
    }

    //will works for all types
    private static Integer getCount(List<?> list) {
        return list.size();
    }
}