package src.main.java.generics.cars;

public class Lamborghini extends Car {

    private String color;

    public Lamborghini(String name, String model, int year, double price, String speed, String color) {
        super(name, model, year, price, speed);
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public String toString() {
        return String.format("Name is %s, model is %s, year is %s, price is %s,  speed is %s, color is %s.",
                getName(), getModel(), getYear(), getPrice(), getSpeed(), getColor());
    }
}