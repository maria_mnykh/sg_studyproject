package src.main.java.generics.cars;

public class Car extends Vehicle {

    private String speed;

    public Car(String name, String model, int year, double price, String speed) {
        super(name, model, year, price);
        this.speed = speed;
    }

    public String getSpeed() {
        return speed;
    }

    public void setSpeed(String speed) {
        this.speed = speed;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public String toString() {
        return String.format("Name is %s, model is %s, year is %s, price is %s, speed is %s.",
                getName(), getModel(), getYear(), getPrice(), getSpeed());
    }
}