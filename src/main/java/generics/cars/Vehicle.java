package src.main.java.generics.cars;

public class Vehicle {

    private String name;
    private String model;
    private int year;
    private double price;

    public Vehicle(String name, String model, int year, double price) {
        this.name = name;
        this.model = model;
        this.year = year;
        this.price = price;
    }

    public int getYear() {
        return year;
    }

    public String getModel() {
        return model;
    }

    public String getName() {
        return name;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public String toString() {
        return String.format("Name is %s, model is %s, year is %s, price is %s.", getName(), getModel(), getYear(), getPrice());
    }

}