package src.main.java.oop.persons;
public class Manager extends Employee {

    private Integer bonus = 0;

    public Manager(String name, int age, String company, Double salary) {
        super(name, age, company, salary);
    }

    public Integer getBonus() {
        return bonus;
    }

    public void setBonus(Integer bonus) {
        this.bonus = bonus;
    }

    @Override
    public Double calculateSalary(Double salary, int numberOfWorkingDays) {
        return (salary * numberOfWorkingDays) + this.bonus;
    }

    @Override
    public String toString() {
        return String.format("Manager is: name = %s, age = %s, company = %s, salary = %s",
                getName(), getAge(), getCompany(), getSalary());
    }
}
