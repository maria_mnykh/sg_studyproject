package src.main.java.oop.persons;

public class Developer extends Employee {

    private String technology;

    public Developer(String name, int age, String company, Double salary, String technology) {
        super(name, age, company, salary);
        this.technology = technology;
    }

    public String getTechnology() {
        return technology;
    }

    public void setTechnology(String technology) {
        this.technology = technology;
    }

    @Override
    public Double calculateSalary(Double salary, int numberOfWorkingDays) {
        if (technology.equals("NodeJS")) {
            return salary * numberOfWorkingDays * 1.5;
        } else {
            return salary * numberOfWorkingDays * 1.3;
        }
    }

    public Double calculateSalary(Double salary, int numberOfWorkingDays, int bonus) {
        if (technology.equals("NodeJS")) {
            return salary * numberOfWorkingDays * 1.5 + bonus;
        } else {
            return salary * numberOfWorkingDays * 1.3 + bonus;
        }
    }

    public Double calculateSalary(Double salary, int numberOfWorkingDays, Double rate) {
        if (technology.equals("NodeJS")) {
            return salary * numberOfWorkingDays * 1.5 * rate;
        } else {
            return salary * numberOfWorkingDays * 1.3 * rate;
        }
    }

    @Override
    public String toString() {
        return String.format("Developer is: name = %s, age = %s, company = %s, salary = %s",
                getName(), getAge(), getCompany(), getSalary());
    }
}