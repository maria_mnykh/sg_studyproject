package src.main.java.oop.persons;

import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;

public class Main {

    public static void main(String[] args) {

        Person david = new Person("David", 23);
        System.out.println(david.getName());
        Person person = new Person("Test", 34);
        System.out.println(person.getName());
//        Employee mark = new Employee("Mark", 30, "Google", 100.0);
//        Manager kate = new Manager("Kate", 45, "Aplle", 150.0);
//        Developer tim = new Developer("Tim", 50, "Google", 250.0, "NodeJS");
//
//        System.out.println(kate);
//        kate.setBonus(300);
//        Double kateSalary = kate.calculateSalary(kate.getSalary(), 21);
//        kate.printSalary(kateSalary);
//
//        System.out.println(tim);
//        Double timSalary = tim.calculateSalary(tim.getSalary(), 18);
//        tim.printSalary(timSalary);


        //Polimorphism
        Developer john = new Developer("John", 34, "SS", 100.0, "JS");

        System.out.println((john instanceof Person));
        System.out.println((john instanceof Employee));
        System.out.println((john instanceof Developer));
        System.out.println((john instanceof Object));
        //System.out.println((john instanceof Manager));

        Person person1 = john;
        Employee employee1 = john;
        //Manager manager1 = john;


        //Upcasting
        // here we create instance of class Employee and store it in variable of Person
        // jane only has methods of Person class
//        Person jane = new Employee("Jane", 34, "SS", 100.0);
//
//        Employee jack = new Manager("Jack", 34, "SS", 100.0);
//
//        Employee ann = new Developer("Ann", 25, "Google", 250.0, "NodeJS");
//
//        //ann.getTechnology();
//
//        System.out.println((ann instanceof Person));
//        System.out.println((ann instanceof Developer));
//        System.out.println((ann instanceof Employee));
//        System.out.println((ann instanceof Manager));
//
//        //Downcasting
//
//        // here instance of Developer is explicitly downcasted to Person
//        Employee employeeJane = (Employee) jane;
//        jane.getName();
//
//        Developer developerAnn = (Developer) ann;
//        developerAnn.getTechnology();


        //Developer developerAnna = new Employee ("Jane", 34, "SS", 100.0);
    }
}
