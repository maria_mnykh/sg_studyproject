package src.main.java.oop.persons;

public class Person{

    private String name;
    private int age;

//    public Person() {
//    }

    public Person(String name, int age) {
        this.name = name;
        this.age = age;
        System.out.println(String.format("New obj is created: %s", this));
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
        //new Person().age = age;
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }

    @Override
    public String toString() {
        return String.format("Person is: name = %s, age = %s", name, age);
    }
}
