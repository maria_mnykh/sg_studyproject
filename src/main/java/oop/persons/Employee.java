package src.main.java.oop.persons;

public class Employee extends Person {

    private String company;

    private Double salary;


    public Employee(String name, int age, String company, Double salary) {
        super(name, age);
        this.company = company;
        this.salary = salary;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Double getSalary() {
        return salary;
    }

    public void setSalary(Double salary) {
        this.salary = salary;
    }

    public Double calculateSalary(Double salary, int numberOfWorkingDays) {
        return salary * numberOfWorkingDays;
    }

    public void printSalary(Double salary) {
        System.out.println(String.format("Employee is %s, total salary is %s", getName(), salary));
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }


    @Override
    public String toString() {
        return String.format("Employee is: name = %s, age = %s, company = %s, salary = %s",
                getName(), getAge(), getCompany(), getSalary());
    }

}