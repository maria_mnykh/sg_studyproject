package src.main.java.oop.animals;

public class Cat extends Animal {

    private String color;

    public Cat(String name, String food, boolean isHungry, String color) {
        super(name, food, isHungry);
        this.color = color;
    }

    public Cat() {
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getColor() {
        return color;
    }

    @Override
    public void eat(String food) {
        if (isHungry()) {
            if (!food.equals("Meat")) {
                System.out.println("I don`t like this food, give me the Meat");
            } else {
                setFood(food);
                System.out.println(String.format("This food %s is good", food));
                setHungry(false);
            }
        }
    }

    @Override
    public void talk() {
        System.out.println(String.format("I am %s, my name is %s I talk meow", getClass().getName(), getName()));
    }

    @Override
    public String toString() {
        return String.format("Cat is: name = %s", getName());
    }
}