package src.main.java.oop.animals;

public class Deer extends Animal implements Vegeterian {

    public Deer(String name, String food, boolean isHungry) {
        super(name, food, isHungry);
    }

    public Deer() {
    }

    @Override
    public void eat(String food) {
        if (isHungry()) {
            if (food.equals("Meat")) {
                System.out.println("I am Vegeterian and can not eat Meat");

            } else {
                setFood(food);
                System.out.println(String.format("Good food for me %s.", food));
                setHungry(false);
            }
        }
    }

    @Override
    public void talk() {
        System.out.println(String.format("I am %s, my name is %s I talk hi", getClass().getName(), getName()));
    }

    @Override
    public boolean isVegetarian() {
        if (getFood().equals("Meat")) {
            return false;
        } else {
            return true;
        }
    }
}