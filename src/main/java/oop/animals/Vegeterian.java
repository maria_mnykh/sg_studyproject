package src.main.java.oop.animals;

public interface Vegeterian {

    boolean isVegetarian();
}