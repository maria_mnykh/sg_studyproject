package src.main.java.oop.animals;

public abstract class Animal {

    private String name;
    private boolean isHungry;
    private String food;

    public String getFood() {
        return food;
    }

    public void setFood(String food) {
        this.food = food;
    }

    public boolean isHungry() {
        return isHungry;
    }

    public void setHungry(boolean hungry) {
        isHungry = hungry;
    }

    public String getName() {
        return name;
    }

    public Animal(String name, String food, boolean isHungry) {
        this.name = name;
        this.food = food;
        this.isHungry = isHungry;
    }

    public Animal() {
    }

    public abstract void eat(String food);

    public abstract void talk();

    @Override
    public String toString() {
        return String.format("Animal is: name = %s", getName());
    }

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }
}