package src.main.java.oop.animals;

public class Main {
    public static void main(String[] args) {
        Animal cat = new Cat("Bill", "Meat", true, "White");
        Animal deer = new Deer("Deer", "Greens", true);
//        Vegeterian deer1 = new Cat();
        Vegeterian deer2 = new Deer();

        System.out.println(cat);
        cat.eat("Greens");
        cat.eat("Meat");
        cat.talk();

        System.out.println(deer);
        deer.eat("Meat");
        deer.eat("Greens");
        deer.talk();

        Deer deer3 = new Deer("Deer", "Meat", true);
        System.out.println(deer3.isVegetarian());

    }
}